package Controlador;

import Modelo.Cotizacion;
import Vista.dlgCotizaciones;
import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private Cotizacion cot;
    private dlgCotizaciones vista;
    private static Boolean isMostrado = false;
    
    public Controlador(dlgCotizaciones vista, Cotizacion cot){
        this.cot = cot;
        this.vista = vista;
        
        //Escuchar los eventos
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
        
    }
    
    private void alternarVisibilidadCalculosBotones(Boolean bool){
        // Mostrar los cálculos
        this.vista.lblPI.setVisible(bool);
        this.vista.lblPM.setVisible(bool);
        this.vista.lblFinanciar.setVisible(bool);
        this.vista.lbltotal.setVisible(bool);
        this.vista.txtPagoInicial.setVisible(bool);
        this.vista.txtPagoMensual.setVisible(bool);
        this.vista.txtTotalFinanciar.setVisible(bool);
        
       // Mostrar los botones
        this.vista.btnLimpiar.setVisible(!bool);
        this.vista.btnCancelar.setVisible(!bool);
        this.vista.btnCerrar.setVisible(!bool);
        this.vista.btnGuardar.setVisible(!bool);
        if(bool)
            this.vista.btnMostrar.setText("<- Regresar");
        else{
            this.vista.btnMostrar.setText("Mostrar Guardado");
            this.limpiar();
        }
    }
    
    public void inicializarVista(){
        this.vista.setTitle(":: Cotizaciones ::");
        this.vista.setSize(414, 656);
        this.alternarVisibilidadCalculosBotones(Boolean.FALSE);
        this.vista.setLocationRelativeTo(null);
        this.vista.setVisible(true);
    }
    
    public static void main(String[] args){
        Cotizacion cot = new Cotizacion();
        dlgCotizaciones vista = new dlgCotizaciones(new JFrame(), true);
        Controlador controlador = new Controlador(vista, cot);
        controlador.inicializarVista();
    }
    
    private void activarComponentes(Boolean bool){
        this.vista.txtNumeroCotizacion.enable(bool);
        this.vista.txtDescripcion.enable(bool);
        this.vista.txtPrecio.enable(bool);
        this.vista.txtPorcentaje.enable(bool);
        this.vista.cmbPlazos.setEnabled(bool);
        this.vista.btnGuardar.setEnabled(bool);
        this.vista.btnMostrar.setEnabled(bool);
        this.vista.btnLimpiar.setEnabled(bool);
        this.vista.btnCancelar.setEnabled(bool);
        this.vista.btnCerrar.setEnabled(bool);
        // Habilitar colores componentes
        if(bool){
            // Txt
            this.vista.txtNumeroCotizacion.setBackground(new Color(242, 242, 242));
            this.vista.txtDescripcion.setBackground(new Color(242, 242, 242));
            this.vista.txtPrecio.setBackground(new Color(242, 242, 242));
            this.vista.txtPorcentaje.setBackground(new Color(242, 242, 242));
            this.vista.cmbPlazos.setBackground(new Color(242, 242, 242));
            // Botones
            this.vista.btnLimpiar.setBackground(new Color(51, 153, 255));
            this.vista.btnCancelar.setBackground(new Color(51, 153, 255));
            this.vista.btnCerrar.setBackground(new Color(255,102,102));
            this.vista.btnGuardar.setBackground(new Color(0,204,102));
            this.vista.btnMostrar.setBackground(new Color(204,0,204));
        }
        else{
            // Txt
            this.vista.txtNumeroCotizacion.setBackground(new Color(220, 220, 220));
            this.vista.txtDescripcion.setBackground(new Color(220, 220, 220));
            this.vista.txtPrecio.setBackground(new Color(220, 220, 220));
            this.vista.txtPorcentaje.setBackground(new Color(220, 220, 220));
            this.vista.cmbPlazos.setBackground(new Color(220, 220, 220));
            // Botones
            this.vista.btnLimpiar.setBackground(new Color(100, 100, 100));
            this.vista.btnCancelar.setBackground(new Color(100, 100, 100));
            this.vista.btnCerrar.setBackground(new Color(100, 100, 100));
            this.vista.btnGuardar.setBackground(new Color(100, 100, 100));
            this.vista.btnMostrar.setBackground(new Color(100, 100, 100));
        }
    }
    
    private void resetearObjeto(){
        this.cot = null;
        this.cot = new Cotizacion();
    }
    
    private void limpiar(){
        this.vista.txtNumeroCotizacion.setText("");
        this.vista.txtDescripcion.setText("");
        this.vista.txtPrecio.setText("");
        this.vista.txtPorcentaje.setText("");
        this.vista.cmbPlazos.setSelectedIndex(0);
        this.vista.txtPagoInicial.setText("");
        this.vista.txtTotalFinanciar.setText("");
        this.vista.txtPagoMensual.setText("");
        this.vista.txtNumeroCotizacion.requestFocusInWindow();
    }
    
    private void reset(){
        this.limpiar();
        this.activarComponentes(Boolean.FALSE);
    }
    
    private Boolean isVacio(){
        return this.vista.txtNumeroCotizacion.getText().equals("") ||
               this.vista.txtDescripcion.getText().equals("") ||
               this.vista.txtPrecio.getText().equals("") ||
               this.vista.txtPorcentaje.getText().equals("");
    }
    
    private int isNegativo(){
        try{
            if(Integer.parseInt(this.vista.txtNumeroCotizacion.getText()) <= 0 ||
               Integer.parseInt(this.vista.txtPrecio.getText()) <= 0 ||
               Integer.parseInt(this.vista.txtPorcentaje.getText()) <= 0){
                return 1;
            }
            return 0;
        }
        catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(this.vista, "Ocurrio el siguiente error: " + ex.getMessage(), ":: Cotizador ::", JOptionPane.ERROR_MESSAGE);
            return 2;
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(!this.vista.isEnabled()){
            System.exit(0);
        }
        if(e.getSource() == this.vista.btnNuevo){
            this.activarComponentes(Boolean.TRUE);
            Controlador.isMostrado = (Controlador.isMostrado) ? !Controlador.isMostrado : false;
            this.alternarVisibilidadCalculosBotones(Boolean.FALSE);
            this.limpiar();
        }
        if(e.getSource() == this.vista.btnLimpiar){
            this.limpiar();
        }
        if(e.getSource() == this.vista.btnCancelar){
            this.reset();
        }
        if(e.getSource() == this.vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(vista, 
            "¿Seguro que deseas salir?", 
            "Productos", 
            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                this.vista.setVisible(false);
                this.vista.dispose();
                System.exit(0);
            }
        }
        if(e.getSource() == this.vista.btnGuardar){
            int porcentaje = 0;
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vista, "Aun quedan campos vacios", ":: Cotizador ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            int neg = this.isNegativo();
            if(neg == 1){
                JOptionPane.showMessageDialog(this.vista, "No se puede operar con números negativos o iguales a 0", ":: Cotizador ::", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(neg == 2) return;
            this.cot.setDescripcionAutomovil(this.vista.txtDescripcion.getText());
            try{
                this.cot.setNumCotizacion(Integer.parseInt(this.vista.txtNumeroCotizacion.getText()));
                this.cot.setPrecio(Float.parseFloat(this.vista.txtPrecio.getText()));
                porcentaje = Integer.parseInt(this.vista.txtPorcentaje.getText());
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio el siguiente error: " + ex.getMessage(), ":: Cotizador ::", JOptionPane.ERROR_MESSAGE);
                return;
            }
            finally{
                if(porcentaje < 101){
                    this.cot.setPorcentajePago(porcentaje);
                }
                else
                {
                    JOptionPane.showMessageDialog(this.vista, "El porcentaje no debe exceder del 100%", ":: Cotizador ::", JOptionPane.WARNING_MESSAGE);
                    this.resetearObjeto();
                    return;
                }
                if(this.vista.cmbPlazos.getSelectedIndex() < 2)
                    this.cot.setPlazo(Integer.parseInt(this.vista.cmbPlazos.getSelectedItem().toString().substring(0, 1)));
                else
                    this.cot.setPlazo(Integer.parseInt(this.vista.cmbPlazos.getSelectedItem().toString().substring(0, 2)));
                JOptionPane.showMessageDialog(this.vista, "Se ha agregado el objeto con exito", ":: Cotizador ::", JOptionPane.INFORMATION_MESSAGE);
                this.limpiar();
            }
            
        }
        if(e.getSource() == this.vista.btnMostrar){
            Controlador.isMostrado = !Controlador.isMostrado;
            this.alternarVisibilidadCalculosBotones(Controlador.isMostrado);
            if(this.vista.btnMostrar.getText().equals("<- Regresar")){
                this.vista.txtNumeroCotizacion.setText(String.valueOf(this.cot.getNumCotizacion()));
                this.vista.txtDescripcion.setText(this.cot.getDescripcionAutomovil());
                this.vista.txtPrecio.setText(String.valueOf(this.cot.getPrecio()));
                this.vista.txtPorcentaje.setText(String.valueOf(this.cot.getPorcentajePago()));
                this.vista.cmbPlazos.setSelectedItem(this.cot.getPlazo() + " Meses");
                this.vista.txtPagoInicial.setText(String.valueOf(this.cot.calcularPagoInicial()));
                this.vista.txtPagoMensual.setText(String.valueOf(this.cot.calcularPagoMensual()));
                this.vista.txtTotalFinanciar.setText(String.valueOf(this.cot.calcularTotalFinanciar()));
            }            
        }     
    }
}