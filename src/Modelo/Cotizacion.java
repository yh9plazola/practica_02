package Modelo;

public class Cotizacion {
    private int numCotizacion;
    private String descripcionAutomovil;
    private float precio;
    private int porcentajePago;
    private int plazo;
    
    // Constructores
    public Cotizacion() {
        this.numCotizacion = 0;
        this.descripcionAutomovil = "";
        this.precio = 0.0f;
        this.porcentajePago = 0;
        this.plazo = 0;
    }

    public Cotizacion(int numCotizacion, String descripcionAutomovil, float precio, int porcentajePago, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precio = precio;
        this.porcentajePago = porcentajePago;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion obj){
        this.numCotizacion = obj.numCotizacion;
        this.descripcionAutomovil = obj.descripcionAutomovil;
        this.precio = obj.precio;
        this.porcentajePago = obj.porcentajePago;
        this.plazo = obj.plazo;
    }
    
    // Metodos Get y Set
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPorcentajePago() {
        return porcentajePago;
    }

    public void setPorcentajePago(int porcentajePago) {
        this.porcentajePago = porcentajePago;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    // Metodos    
    public float calcularPagoInicial(){
        return this.precio * ((float)this.porcentajePago / 100);
    }
    
    public float calcularTotalFinanciar(){
        return this.precio - this.calcularPagoInicial();
    }
    
    public float calcularPagoMensual(){
        return this.calcularTotalFinanciar() / (float)this.plazo;
    }
    
    @Override
    public String toString(){
        return "Cotizacion:\n\nNumero de Cotizacion: " + this.numCotizacion + 
               "\nDescripcion del Automovil: " + this.descripcionAutomovil +
               "\nPrecio: " + this.precio +
               "\nPorcentaje de Pago: " + this.porcentajePago +
               "\nPlazo: " + this.plazo + "\n";
    }
}